from demo_pytest_coverage import suma, resta, multiplicacion, division, saludar, saludar_con_ifs, despedir
import pytest

def test_suma():
    assert suma(2, 3) == 5
    assert suma(-1, 1) == 0

def test_resta():
    assert resta(5, 2) == 3
    assert resta(10, 5) == 5

def test_multiplicacion():
    assert multiplicacion(3, 4) == 12
    assert multiplicacion(-2, 5) == -10

def test_division():
    assert division(10, 2) == 5
    assert division(8, 4) == 2

    with pytest.raises(ValueError):
        division(10, 0)

def test_saludar():
    assert saludar("Juan") == "Hola, Juan!"
    assert saludar("Maria") == "Hola, Maria!"


def test_saludar_con_ifs():
    assert saludar_con_ifs("jonathan") == "Hola, jonathan!"

# La función de despedir debería de decir: Adios, jonathan 
def test_despedir():
    assert despedir("jonathan") == "Adios, jonathan!"