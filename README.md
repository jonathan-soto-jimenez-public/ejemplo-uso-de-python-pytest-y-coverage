# Ejemplo de pruebas unitarias y coverage con python

Este proyecto contiene funciones sencillas para ejemplificar el uso de pytest y coverage.

Cada proyecto, independientemente del tipo de tecnología o propósito,
tiene funciones para realizar acciones.

Como buena práctica de desarrollo, se recomienda tener funciones de único propósito,
es decir, que solo hagan una y solo una cosa.
Después, en funciones más generales, se mandan a llamar estas subfunciones para realizar el flujo deseado.

Gracias a esta practica y en conjunto de las pruebas unitarias y coverage,
podremos validar cada parte de nuestro proyecto de forma automática y generar un reporte de cobertura.

* Las pruebas unitarias ayudarán a saber si el código funciona correctamente.

* El reporte de coverage ayudará a saber si las pruebas unitarías desarrolladas están abarcando todos los aspectos funcionales de tu código.

Sabemos que hay funciones que por más sencillas que las deseemos hacer,
terminan siendo un poco grandes, con varios caminos para el flujo
de acuerdo a condicionales.

Es por eso que coverage nos ayuda a saber si todos los posibles caminos están siendo validados.
## Instalación

Para probar este proyecto, seguir los siguientes pasos, se deberá de tener python instalado:

### Creamos el ambiente virtual
```bash
    py -m venv env
```

### Activamos el ambiente (windows)
```bash
    ./env/Scripts/activate
```

### Instalamos librerias
```bash
    pip install pytest coverage
```


    
## Ejecución de pruebas unitarias

Para ejecutar las pruebas unitarias, correr el siguiente comando:

```bash
    pytest
```

### Para ejecutar coverage con las pruebas unitarias y generar reporte en html:

```bash
    coverage run -m pytest
    coverage report -m
    coverage html
```
## Authors

- [@jonathansj](https://gitlab.com/jonathansj)


## Documentation

[Pytest](https://docs.pytest.org/en/8.0.x/)
[Coverage](https://coverage.readthedocs.io/en/7.4.3/index.html)

