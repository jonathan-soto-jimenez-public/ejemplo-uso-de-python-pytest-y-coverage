def suma(a, b):
    return a + b

def resta(a, b):
    return a - b

def multiplicacion(a, b):
    return a * b

def division(a, b):
    if b == 0:
        raise ValueError("No se puede dividir por cero")
    return a / b

def saludar(nombre):
    return f"Hola, {nombre}!"

# Esta función tiene un error, en vez de decir Hola debería de decir Adios
# pero es para validarlo en la prueba unitaria.
# Para ejemplificar cuando se ha cambiado una función y la prueba unitaria
# nos ayuda a saber que no esta regresando lo que debería.
def despedir(nombre):
    return f"Hola, {nombre}!"

# Esta función solo tiene pruebas unitarias para el primer if, ayudará a validar el coverage.
def saludar_con_ifs(nombre):
    if nombre.__contains__("jon"):
        print("¡Vi que contiene la palabra jon!")
    elif nombre.__contains__("juan"):
        print("¡Vi que contiene la palabra juan!")
    return f"Hola, {nombre}!"


# Esta función no tiene pruebas unitarias
def saludar_y_despedir(nombre):
    return f"Hola y adios, {nombre}!"